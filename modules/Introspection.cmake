# define a set of string with may-be useful readable name this file is meant to
# be included in a CMakeLists.txt not as a standalone CMake script.
#
# The set of variable produced are the following:
# DISTRO_ID       Linux distro as set from lsb_release -i
# DISTRO_CMD_INSTALL the distro specific install command
# DISTRO_RELEASE  release as set from lsb_release -r
# DISTRO_CODENAME distro release codename as set from lsb_release -c
# DISTRO_TAG      the tag appended to the full package name
# DISTRO_LIB_INSTALL_DIR  depending by the combination of the OS
#                 architecture AND the distro it will be the installation 
#                 suffix relative path to the proper lib
# DISTRO_CPACK_GENERATOR the default package generator of the distro

# In the Linux case try to guess the distro name/type
# using lsb_release program
if(NOT CMAKE_SYSTEM_NAME MATCHES "Linux")
  message(FATAL_ERROR "Only Linux system are supported")
endif()

set(DISTRO_LIB_INSTALL_DIR "lib")

find_program(LSB_RELEASE_EXECUTABLE lsb_release)
if (LSB_RELEASE_EXECUTABLE)
  execute_process(COMMAND ${LSB_RELEASE_EXECUTABLE} -i
      OUTPUT_VARIABLE _TMP_LSB_RELEASE_OUTPUT
      ERROR_QUIET
      OUTPUT_STRIP_TRAILING_WHITESPACE)
#   message("'${_TMP_LSB_RELEASE_OUTPUT}'")
  string(REGEX MATCH "Distributor ID:(.*)" DISTRO_ID ${_TMP_LSB_RELEASE_OUTPUT})
  string(STRIP "${CMAKE_MATCH_1}" DISTRO_ID)
  # replace potential space with underscore
  string(REPLACE " " "_" DISTRO_ID "${DISTRO_ID}")
  execute_process(COMMAND ${LSB_RELEASE_EXECUTABLE} -r
      OUTPUT_VARIABLE _TMP_LSB_RELEASE_OUTPUT
      ERROR_QUIET
      OUTPUT_STRIP_TRAILING_WHITESPACE)
  string(REGEX MATCH "Release:(.*)" DISTRO_RELEASE ${_TMP_LSB_RELEASE_OUTPUT})
  string(STRIP "${CMAKE_MATCH_1}" DISTRO_RELEASE)
  execute_process(COMMAND ${LSB_RELEASE_EXECUTABLE} -c
      OUTPUT_VARIABLE _TMP_LSB_RELEASE_OUTPUT
      ERROR_QUIET
      OUTPUT_STRIP_TRAILING_WHITESPACE)
  string(REGEX MATCH "Codename:(.*)" DISTRO_CODENAME ${_TMP_LSB_RELEASE_OUTPUT})
  string(STRIP "${CMAKE_MATCH_1}" DISTRO_CODENAME)
  string(REPLACE "n/a" "" DISTRO_RELEASE "${DISTRO_RELEASE}")
  string(REPLACE "n/a" "" DISTRO_CODENAME "${DISTRO_CODENAME}")
else (LSB_RELEASE_EXECUTABLE)
  message(FATAL_ERROR "${LSB_RELEASE_EXECUTABLE} not found. Please install lsb-release package")
endif(LSB_RELEASE_EXECUTABLE)
# Now mangle some names
if (DISTRO_ID MATCHES "CentOS|Debian|Fedora|SUSE|Gentoo|Ubuntu")
  ###################################################
  # common / default properties
  set(DISTRO_ARCH "${CMAKE_SYSTEM_PROCESSOR}")
  ###################################################
  # FEDORA
  if    (DISTRO_ID MATCHES "Fedora")
    set(DISTRO_CPACK_GENERATOR "RPM")
    set(DISTRO_PSUFFIX "rpm")
    set(DISTRO_CMD_INSTALL "dnf install -y")
    set(DISTRO_TAG ".fc${DISTRO_RELEASE}")
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
      set(DISTRO_LIB_INSTALL_DIR "lib64")
    endif(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  ###################################################
  # SUSE
  elseif (DISTRO_ID MATCHES "SUSE")
    set(DISTRO_CPACK_GENERATOR "RPM")
    set(DISTRO_PSUFFIX "rpm")
    set(DISTRO_CMD_INSTALL "rpm -ivh")
    string(REGEX MATCH "([0-9]+)\\.([0-9]+)" REL "${DISTRO_RELEASE}")
    set(DISTRO_TAG ".suse${CMAKE_MATCH_1}_${CMAKE_MATCH_2}")
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
      set(DISTRO_LIB_INSTALL_DIR "lib64")
    endif(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  ###################################################
  # CENTOS
  elseif (DISTRO_ID MATCHES "CentOS")
    set(DISTRO_CPACK_GENERATOR "RPM")
    set(DISTRO_PSUFFIX "rpm")
    set(DISTRO_CMD_INSTALL "rpm -ivh")
    string(REGEX MATCH "([0-9]+)\\..*" REL "${DISTRO_RELEASE}")
    set(DISTRO_TAG ".el${CMAKE_MATCH_1}")
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
      set(DISTRO_LIB_INSTALL_DIR "lib64")
    endif(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  ###################################################
  # DEBIAN
  elseif (DISTRO_ID MATCHES "Debian")
    set(DISTRO_CPACK_GENERATOR "DEB")
    set(DISTRO_PSUFFIX "deb")
    set(DISTRO_CMD_INSTALL "dpkg -i")
    set(DISTRO_TAG "1")
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
      set(DISTRO_ARCH "amd64")
    endif(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  elseif (DISTRO_ID MATCHES "Ubuntu")
    set(DISTRO_CPACK_GENERATOR "DEB")
    set(DISTRO_PSUFFIX "deb")
    set(DISTRO_CMD_INSTALL "dpkg -i")
    set(DISTRO_TAG "0ubuntu1")
    if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
      set(DISTRO_ARCH "amd64")
    endif(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  ###################################################
  # GENTOO
  elseif(DISTRO_ID MATCHES "Gentoo")
    set(DISTRO_CPACK_GENERATOR "TGZ")
    set(DISTRO_PSUFFIX "tgz")
    set(DISTRO_TAG ".gentoo${DISTRO_RELEASE}")
    set(DISTRO_ARCH "${CMAKE_SYSTEM_PROCESSOR}")
  ###################################################
  # ERROR
  else (DISTRO_ID MATCHES "CentOS|Debian|Fedora|SUSE|Gentoo")
    message(FATAL_ERROR "Distro '${DISTRO_ID}' unsupported")
  endif(DISTRO_ID MATCHES "Fedora")
endif(DISTRO_ID MATCHES "CentOS|Debian|Fedora|SUSE|Gentoo|Ubuntu")

message("++ Linux distro  : ${DISTRO_ID}")
message("++   release     : ${DISTRO_RELEASE}")
message("++   tag         : ${DISTRO_TAG}")
message("++   code name   : ${DISTRO_CODENAME}")
message("++   lib target  : ${DISTRO_LIB_INSTALL_DIR}")
message("++   cpack gen.  : ${DISTRO_CPACK_GENERATOR}")
