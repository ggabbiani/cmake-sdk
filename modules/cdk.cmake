#
# CMake Software Development Kit
#
# This file is part of the 'CMake Software Development Kit' (CDK) project.
#
# Copyright © 2022, Giampiero Gabbiani (giampiero@gabbiani.org)
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

# print a cmake message prefixed with the calling environment project name
macro(cdk_message mode)
  set(__opts__)
  set(__ones__)
  set(__multi__)
  cmake_parse_arguments(arg "${__opts__}" "${__ones__}" "${__multi__}" ${ARGN})
  string(TOUPPER ${PROJECT_NAME} __project__)
  message(${mode} "${__project__}: " ${arg_UNPARSED_ARGUMENTS})
endmacro(cdk_message)

###############################################################################
# git_branch_version(VARIABLE «variable name» [VERBOSE])
#
# sets GIT_BRANCH_VERSION to the value of the last git tag
###############################################################################
function(git_branch_version)
  set(options VERBOSE)
  set(oneValueArgs VARIABLE WORKING_DIRECTORY)
  set(multiValueArgs)
  cmake_parse_arguments(arg "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  if (NOT arg_VARIABLE)
    message(FATAL_ERROR "Missing output variable name")
  endif()
  if (NOT arg_WORKING_DIRECTORY)
    message(FATAL_ERROR "Missing working directory")
  endif()
  execute_process(
    COMMAND ${GIT_EXECUTABLE} describe --abbrev=0
    OUTPUT_VARIABLE version
    COMMAND_ERROR_IS_FATAL ANY
    OUTPUT_STRIP_TRAILING_WHITESPACE
    WORKING_DIRECTORY "${arg_WORKING_DIRECTORY}"
  )
  # ignore leading "v" from version
  string(SUBSTRING ${version} 1 -1 version)

  # set version as a list
  string(REPLACE "." ";" version_list ${version})
  # get versioning level number
  list(LENGTH version_list length)
  if (NOT length)
    message(FATAL_ERROR "Bad version string '${version}'")
  endif()
  list(GET version_list 0 version_major)

  if (length GREATER 1)
    list(GET version_list 1 version_minor)
  else()
    set(version_minor 0)
  endif()

  if (length GREATER 2)
    list(GET version_list 2 version_patch)
  else()
    set(version_patch 0)
  endif()

  execute_process(
    COMMAND ${GIT_EXECUTABLE} log -1 --pretty=format:%h
    OUTPUT_VARIABLE commit
    COMMAND_ERROR_IS_FATAL ANY
    OUTPUT_STRIP_TRAILING_WHITESPACE
    WORKING_DIRECTORY "${arg_WORKING_DIRECTORY}"
  )
  execute_process(
    COMMAND ${GIT_EXECUTABLE} status -s
    OUTPUT_VARIABLE status
    COMMAND_ERROR_IS_FATAL ANY
    OUTPUT_STRIP_TRAILING_WHITESPACE
    WORKING_DIRECTORY "${arg_WORKING_DIRECTORY}"
  )
  string(REGEX REPLACE " M " "" status "${status}")
  string(REGEX REPLACE "[\r\n]" ";" status "${status}")
  list(LENGTH status len)
  if (len GREATER 0)
    set(commit "${commit}+")
  endif()

  set(${arg_VARIABLE}_COMMIT ${commit} PARENT_SCOPE)
  set(${arg_VARIABLE}_MAJOR ${version_major} PARENT_SCOPE)
  set(${arg_VARIABLE}_MINOR ${version_minor} PARENT_SCOPE)
  set(${arg_VARIABLE}_PATCH ${version_patch} PARENT_SCOPE)
  set(${arg_VARIABLE} "${version_major}.${version_minor}.${version_patch}" PARENT_SCOPE)
  if (arg_VERBOSE)
    cmake_print_variables(version_major version_minor version_patch commit)
  endif()
endfunction(git_branch_version)

# setup a variable ${arg_VARIABLE}_REPO_NAME with the name of the git repository
function(git_repo_name)
  set(opts)
  set(ones VARIABLE WORKING_DIRECTORY)
  set(multi)
  cmake_parse_arguments(arg "${opts}" "${ones}" "${multi}" ${ARGN})
  if (NOT arg_VARIABLE)
    message(FATAL_ERROR "Missing output variable name")
  endif()
  if (NOT arg_WORKING_DIRECTORY)
    message(FATAL_ERROR "Missing working directory")
  endif()
  execute_process(
    COMMAND ${GIT_EXECUTABLE} config --get remote.origin.url
    OUTPUT_VARIABLE repo_name
    WORKING_DIRECTORY ${arg_WORKING_DIRECTORY}
    COMMAND_ERROR_IS_FATAL ANY
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  cmake_path(GET repo_name STEM git_repo_name)
  set(${arg_VARIABLE} ${git_repo_name} PARENT_SCOPE)
endfunction(git_repo_name)

# sets the number of commits since last tag in the passed VARIABLE name
#
# git describe --tags returns two kind of output depending on the git repo status:
# x.y.x (in case of tagged commit)
# x.y.z-n-hhhhhhhh (n-th commit after x.y.z tag, hash hhhhhhhh)
# Returns «n» in the second case and 0 in the first.
# it is assumed that the version of the package is aligned with the last repo tag
function(git_commits)
  set(opts)
  set(ones VARIABLE WORKING_DIRECTORY)
  set(multi)
  cmake_parse_arguments(arg "${opts}" "${ones}" "${multi}" ${ARGN})
  if (NOT arg_VARIABLE)
    message(FATAL_ERROR "Missing output variable name")
  endif()
  if (NOT arg_WORKING_DIRECTORY)
    message(FATAL_ERROR "Missing working directory")
  endif()
  set(GIT_COMMAND ${GIT_EXECUTABLE})
  # check if GIT_EXECUTABLE is a python2 script and change it accordingly
  if (MSYS OR MINGW)
    execute_process(
      COMMAND file ${GIT_EXECUTABLE}
      WORKING_DIRECTORY ${arg_WORKING_DIRECTORY}
      OUTPUT_STRIP_TRAILING_WHITESPACE
      OUTPUT_VARIABLE git_file_type
    )
    if ("${git_file_type}" MATCHES "Python script")
      set(GIT_COMMAND python2 ${GIT_EXECUTABLE})
    endif()
  endif()
  execute_process(
    COMMAND ${GIT_COMMAND} describe --tags --long
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE VERSION_TAG
  )
  string(REGEX MATCHALL "^[^-]*-([0-9]*)-.*$" dummy ${VERSION_TAG})
  if (CMAKE_MATCH_COUNT)
    set(${arg_VARIABLE} "${CMAKE_MATCH_1}" PARENT_SCOPE)
  else()
    set(${arg_VARIABLE} 0 PARENT_SCOPE)
  endif()
endfunction(git_commits)

function(__chk__)
  set(opts TO_CMAKE_PATH TO_NATIVE_PATH)
  set(ones  )
  set(multi )
  cmake_parse_arguments(arg "${opts}" "${ones}" "${multi}" ${ARGN})
  set(value ${arg_UNPARSED_ARGUMENTS})
  if (NOT value)
    message(FATAL_ERROR "Value missing")
  endif()

  if (arg_TO_CMAKE_PATH)
    file(TO_CMAKE_PATH "${value}" __chk_result__)
    set(__chk_result__ "${__chk_result__}" PARENT_SCOPE)
  elseif (arg_TO_NATIVE_PATH)
    file(TO_NATIVE_PATH "${value}" __chk_result__)
    set(__chk_result__ "${__chk_result__}" PARENT_SCOPE)
  else()
    set(__chk_result__ "${value}" PARENT_SCOPE)
  endif()
endfunction(__chk__)

function(set_if variable)
  set(opts  )
  set(ones  )
  set(multi IF_APPLE IF_UNIX IF_WIN OTHERWISE)
  cmake_parse_arguments(arg "${opts}" "${ones}" "${multi}" ${ARGN})
  if (NOT variable)
    message(FATAL_ERROR "Variable name missing")
  endif()

  if (arg_IF_WIN AND WIN32)
    __chk__(${arg_IF_WIN})
    set(${variable} ${__chk_result__} PARENT_SCOPE)
    set(__otherwise__ FALSE)
  else()
    set(__otherwise__ TRUE)
  endif()

  if (arg_IF_UNIX AND UNIX)
    __chk__(${arg_IF_UNIX})
    set(${variable} ${__chk_result__} PARENT_SCOPE)
    set(__otherwise__ FALSE)
  else()
    set(__otherwise__ TRUE)
  endif()

  if (arg_IF_APPLE AND APPLE)
    __chk__(${arg_IF_APPLE})
    set(${variable} ${__chk_result__} PARENT_SCOPE)
    set(__otherwise__ FALSE)
  else()
    set(__otherwise__ TRUE)
  endif()

  if (arg_OTHERWISE AND __otherwise__)
    __chk__(${arg_OTHERWISE})
    set(${variable} ${__chk_result__} PARENT_SCOPE)
  endif()
endfunction(set_if)

macro(cdk_setup)
  set(opts)
  set(ones VARIABLE WORKING_DIRECTORY)
  set(multi)
  cmake_parse_arguments(arg "${opts}" "${ones}" "${multi}" ${ARGN})
  # cmake_print_variables(
  #   arg_WORKING_DIRECTORY
  #   arg_VARIABLE
  # )
  if (NOT arg_VARIABLE)
    set(arg_VARIABLE GIT)
    message(STATUS "Output variable name set to ${arg_VARIABLE}")
  endif()
  if (NOT arg_WORKING_DIRECTORY)
    message(FATAL_ERROR "Missing working directory")
  endif()
  git_branch_version(VARIABLE ${arg_VARIABLE}_BRANCH_VERSION WORKING_DIRECTORY "${arg_WORKING_DIRECTORY}")
  git_repo_name(VARIABLE ${arg_VARIABLE}_REPO_NAME WORKING_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}")
endmacro(cdk_setup)

function(cdk_report items)
  cdk_message(STATUS "install prefix: '${CMAKE_INSTALL_PREFIX}'")
  list(APPEND CMAKE_MESSAGE_INDENT "   ")
  set(_max_len 0)
  foreach(p IN LISTS items)
    string(LENGTH "${p}" _len)
    if (_len GREATER _max_len)
      set(_max_len ${_len})
    endif()
  endforeach()
  foreach(p IN LISTS items)
    file(TO_NATIVE_PATH ${CMAKE_INSTALL_${p}DIR} _path )
    string(LENGTH "${p}" _len)
    math(EXPR _e ${_max_len}-${_len})
    string(REPEAT " " ${_e} _spaces)
    message("${p}${_spaces}: ${_path}")
    unset(_path)
  endforeach()
  list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction(cdk_report)

find_package(Git REQUIRED)
